﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyScript : MonoBehaviour {
    bool goUp;
    public int value;

	// Use this for initialization
	void Start () {
        StartCoroutine(SwitchDirection());
        Invoke("distruggi", 20);
	}
	
	// Update is called once per frame
	void Update () {
        if (goUp)
            transform.position = transform.position + new Vector3(0, 0.15f * Time.deltaTime, 0);
        else
           transform.position = transform.position - new Vector3(0, 0.15f * Time.deltaTime, 0);
        transform.Rotate(Vector3.up, 20 * Time.deltaTime, Space.World);
    }

    IEnumerator SwitchDirection()
    {
        while (gameObject.activeSelf)
        {
            yield return new WaitForSeconds(0.5f);
            goUp = !goUp;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            OnPicked(other);
        }
    }

    protected virtual void OnPicked(Collider other)
    {
        ManagerPlayer managerplayer = other.GetComponent<ManagerPlayer>();
        if (managerplayer)
        {
            ManagerPlayer.money = value + ManagerPlayer.money;
        }
        AudioSource AudioMoney = other.GetComponent<AudioSource>();
        AudioMoney.Play();
        Destroy(gameObject);
    }

    void distruggi()
    {
        Destroy(gameObject);
    }
}
