﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    public Transform player;
    
    // Start is called before the first frame update
    private void LateUpdate()
    {
        transform.rotation = Quaternion.Euler(90f, player.eulerAngles.y, 0f);
    }
}
