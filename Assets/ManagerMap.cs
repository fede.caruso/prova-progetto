﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerMap : MonoBehaviour {

    public static ManagerMap singleton;

    void Awake()
    {
        //autodistruggo la Mappa nel caso in cui se ne dovese spawnare un altra
        if ((singleton != null) && (singleton != this))
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
        //non distruggo l' oggetto Player al caricamento di una nuova scena
        DontDestroyOnLoad(gameObject);
    }
}
