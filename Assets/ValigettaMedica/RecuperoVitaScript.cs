﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecuperoVitaScript : MonoBehaviour
{
    bool goUp;
    public int value;
    AudioSource audioRecuperoVita;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SwitchDirection());
        audioRecuperoVita = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (goUp)
        {
            transform.position = transform.position + new Vector3(0, 0.15f * Time.deltaTime, 0);
        }
        else
        {
            transform.position = transform.position - new Vector3(0, 0.15f * Time.deltaTime, 0);
        }
    }

    IEnumerator SwitchDirection()
    {
        while (gameObject.activeSelf)
        {
            yield return new WaitForSeconds(1f);
            goUp = !goUp;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            OnPicked(other);
        }
    }

    public virtual void OnPicked(Collider other)
    {
        ManagerPlayer managerplayer = other.GetComponent<ManagerPlayer>();
        if (managerplayer)
        {
            managerplayer.health = value + managerplayer.health;
        }
        audioRecuperoVita.Play();
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        Invoke("distruggi", 1f);
    }
    void distruggi()
    {
        Destroy(gameObject);
    }
}
