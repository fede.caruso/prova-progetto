﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopAmmo : MonoBehaviour
{
    public int prezzo = 200;
    public GameObject pnlShop;
    public AudioSource AcquistoAmmo;
    public AudioClip NoMoney;
    public GameObject Light;
    bool entrato;

    // Update is called once per frame
    void Update()
    {
        if (PausaScript.GameIsPaused)
        {
            pnlShop.SetActive(false);
        }
        else if (entrato)
        {
            pnlShop.SetActive(true);
        }

        if (entrato)
        {
            if (Input.GetKeyUp(KeyCode.C))
            {
                if (ManagerPlayer.money >= prezzo)
                {
                    ManagerPlayer.money -= prezzo;
                    ManagerPlayer.ammo += 200;
                    AcquistoAmmo.Play();
                    Debug.Log("comprato");
                }
                else AcquistoAmmo.PlayOneShot(NoMoney);
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            pnlShop.SetActive(true);
            Light.SetActive(false);
            entrato = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            pnlShop.SetActive(false);
            Light.SetActive(true);
            entrato = false;
        }
    }
}
