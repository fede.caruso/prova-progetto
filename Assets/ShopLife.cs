﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopLife : MonoBehaviour
{
    public int prezzo = 1000;
    public GameObject pnlShop;
    public ManagerPlayer playerscript;
    public AudioSource AcquistoVita;
    public AudioClip NoMoney;
    public GameObject Light;
    bool entrato;

    // Start is called before the first frame update
    void Start()
    {
        playerscript = ManagerPlayer.singleton.gameObject.GetComponent<ManagerPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PausaScript.GameIsPaused)
        {
            pnlShop.SetActive(false);
        }
        else if (entrato)
        {
            pnlShop.SetActive(true);
        }

        if (entrato)
        {
            if (Input.GetKeyUp(KeyCode.C))
            {
                if (ManagerPlayer.money >= prezzo)
                {
                    ManagerPlayer.money -= prezzo;
                    playerscript.health = 100;
                    AcquistoVita.Play();
                }
                else AcquistoVita.PlayOneShot(NoMoney);
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            pnlShop.SetActive(true);
            Light.SetActive(false);
            entrato = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            pnlShop.SetActive(false);
            Light.SetActive(true);
            entrato = false;
        }
    }
}