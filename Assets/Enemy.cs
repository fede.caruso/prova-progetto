﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using System.Reflection;
using UnityEngine.Scripting;

public class Enemy : MonoBehaviour
{
    AudioSource audioSource;
    Animator anim;
    [SerializeField]
    NavMeshAgent agent;
    GameObject player;
    ManagerPlayer playerscript;
    Transform targetToFollow;
    Vector3 posizioneEnemy;
    int maxhealth;
    public int health;
    public Slider healthbar;
    public float damage;
    public int point;
    public GameObject money;
    public ParticleSystem sangue;
    public bool deathPlayer = false;

    float currenthealthpercentage
    {
        get
        {
            return ((float)(health) / (float)(maxhealth));
        }
    }
    
    private void OnValidate()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        targetToFollow = GameObject.FindWithTag("PivotPlayer").transform;
        maxhealth = health;
        player = GameObject.FindWithTag("Player");
        damage = (damage * 0.008f);
        playerscript = player.GetComponent<ManagerPlayer>();
        audioSource = GetComponent<AudioSource>();
        sangue = gameObject.GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        //stoppo audio in pausa
        if (PausaScript.GameIsPaused)
            audioSource.Pause();
        else if (PausaScript.GameIsPaused == false)
            audioSource.UnPause();
        
        //quando zombie è in vita
        if (health > 0 && deathPlayer == false)
        {
            float distanza = Vector3.Distance(this.transform.position, targetToFollow.position);
            if (distanza > 100f)
            {
                agent.SetDestination(this.transform.position);
                anim.SetBool("cammina", false);
            }
            else if (distanza > 1.5f && distanza < 100f)
            {
                agent.SetDestination(targetToFollow.position);
                anim.SetBool("cammina", true);
                anim.SetBool("attacco", false);
            }
            else if(distanza <=1.5f)
            {
                transform.LookAt(targetToFollow);
                anim.SetBool("cammina", false);
                anim.SetBool("attacco", true);
                danneggiaplayer();
            }
        }
        else if(health > 0 && deathPlayer == true)
        {
            agent.enabled = false;
        }
        else
            morte();


        //settaggio e colore barra Zombie  
        healthbar.value = currenthealthpercentage;

        if (healthbar.value > 0.75)
        {
            healthbar.fillRect.GetComponent<Image>().color = new Color(0.2f, 0.8f, 0.2f);
        }
        else if (healthbar.value <= 0.75 && healthbar.value > 0.50)
        {
            healthbar.fillRect.GetComponent<Image>().color = new Color(0.13f, 0.55f, 0.13f);
        }
        else if (healthbar.value <= 0.50 && healthbar.value > 0.25)
        {
            healthbar.fillRect.GetComponent<Image>().color = new Color(1.0f, 0.4f, 0.0f);
        }
        else healthbar.fillRect.GetComponent<Image>().color = new Color(1.0f, 0.0f, 0.0f);

        if (playerscript.health <= 0)
        {
            deathPlayer = true;
            anim.SetBool("attacco", false);
            anim.SetBool("cammina", false);
            audioSource.Stop();
        }
    }

    public void morte()
    {
        GetComponent<Collider>().enabled = false;
        health = 0;
        posizioneEnemy = gameObject.transform.position;
        agent.enabled = false;
        anim.SetBool("colpito", true);
        audioSource.Stop();
        damage = 0.0f;
        Invoke("distruggi", 3);
    }

    void distruggi()
    {
        GestioneScene.Nuccisioni++;
        Debug.Log("Uccisioni " + GestioneScene.Nuccisioni);
        Destroy(gameObject);
        Instantiate(money, posizioneEnemy + money.transform.position, money.transform.rotation);
    }

    public void danneggiaplayer()
    {
        if(PausaScript.GameIsPaused == false)
        {
            playerscript.health = playerscript.health - damage;
            if (playerscript.health <= 0)
            {
                playerscript.health = 0;
            } 
        }
    }
}

    

