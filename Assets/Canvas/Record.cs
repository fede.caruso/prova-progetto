﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Record : MonoBehaviour {
    string MigliorGiocatore;
    int MigliorPunteggio;
    string AttualeGiocatore;
    int AttualePunteggio;

    // Use this for initialization
    void Start () {
        AttualeGiocatore = PlayerPrefs.GetString("lastname");
        AttualePunteggio = PlayerPrefs.GetInt("lastpoint");
        MigliorGiocatore = PlayerPrefs.GetString("pos1name");
        MigliorPunteggio = PlayerPrefs.GetInt("pos1punti");
        
        //Scrivo fisicamente il miglior giocatore e il miglior punteggio nel panel
        if(AttualePunteggio > MigliorPunteggio)
            gameObject.transform.GetChild(4).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = AttualeGiocatore + " " + AttualePunteggio;
        else
        gameObject.transform.GetChild(4).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = MigliorGiocatore + " " + MigliorPunteggio;
	}
}
