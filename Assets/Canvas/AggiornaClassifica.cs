﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AggiornaClassifica : MonoBehaviour {
    public static AggiornaClassifica singleton;  //imposto AggiornaClassifica come singleton
    public string ultimogiocatore;
    public int ultimopunteggio;
    public string data;
    public string pos1giocatore;
    public int pos1punti;
    public string pos1data;
    string pos2giocatore;
    int pos2punti;
    string pos2data;
    string pos3giocatore;
    int pos3punti;
    string pos3data;
    string pos4giocatore;
    int pos4punti;
    string pos4data;
    string pos5giocatore;
    int pos5punti;
    string pos5data;

    void Awake()
    {
        
        if ((singleton != null) && (singleton != this))
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        Aggiorna();
    }

    // Update is called once per frame
    void Update () {
        Debug.Log(ultimogiocatore + " " + ultimopunteggio + " " + data);
    }

    public void Aggiorna()
    {
            ultimogiocatore = PlayerPrefs.GetString("lastname");
            ultimopunteggio = PlayerPrefs.GetInt("lastpoint");
            data = PlayerPrefs.GetString("lastdate");

            pos1giocatore = PlayerPrefs.GetString("pos1name");
            pos1punti = PlayerPrefs.GetInt("pos1punti");
            pos1data = PlayerPrefs.GetString("pos1data");
            pos2giocatore = PlayerPrefs.GetString("pos2name");
            pos2punti = PlayerPrefs.GetInt("pos2punti");
            pos2data = PlayerPrefs.GetString("pos2data");
            pos3giocatore = PlayerPrefs.GetString("pos3name");
            pos3punti = PlayerPrefs.GetInt("pos3punti");
            pos3data = PlayerPrefs.GetString("pos3data");
            pos4giocatore = PlayerPrefs.GetString("pos4name");
            pos4punti = PlayerPrefs.GetInt("pos4punti");
            pos4data = PlayerPrefs.GetString("pos4data");
            pos5giocatore = PlayerPrefs.GetString("pos5name");
            pos5punti = PlayerPrefs.GetInt("pos5punti");
            pos5data = PlayerPrefs.GetString("pos5data");

            if (ultimopunteggio > pos1punti)
            {
                //Scalo di una posizione i nome giocatore
                pos5giocatore = pos4giocatore;
                PlayerPrefs.SetString("pos5name", pos5giocatore);
                pos4giocatore = pos3giocatore;
                PlayerPrefs.SetString("pos4name", pos4giocatore);
                pos3giocatore = pos2giocatore;
                PlayerPrefs.SetString("pos3name", pos3giocatore);
                pos2giocatore = pos1giocatore;
                PlayerPrefs.SetString("pos2name", pos2giocatore);
                pos1giocatore = ultimogiocatore;
                PlayerPrefs.SetString("pos1name", pos1giocatore);

                //Scalo di una posizione i punteggi 
                pos5punti = pos4punti;
                PlayerPrefs.SetInt("pos5punti", pos5punti);
                pos4punti = pos3punti;
                PlayerPrefs.SetInt("pos4punti", pos4punti);
                pos3punti = pos2punti;
                PlayerPrefs.SetInt("pos3punti", pos3punti);
                pos2punti = pos1punti;
                PlayerPrefs.SetInt("pos2punti", pos2punti);
                pos1punti = ultimopunteggio;
                PlayerPrefs.SetInt("pos1punti", pos1punti);

                //Scalo di una posizione la data
                pos5data = pos4data;
                PlayerPrefs.SetString("pos5data", pos5data);
                pos4data = pos3data;
                PlayerPrefs.SetString("pos4data", pos4data);
                pos3data = pos2data;
                PlayerPrefs.SetString("pos3data", pos3data);
                pos2data = pos1data;
                PlayerPrefs.SetString("pos2data", pos2data);
                pos1data = data;
                PlayerPrefs.SetString("pos1data", pos1data);
            }
            else if (ultimopunteggio > pos2punti)
            {
                //Scalo di una posizione i nome giocatore
                pos5giocatore = pos4giocatore;
                PlayerPrefs.SetString("pos5name", pos5giocatore);
                pos4giocatore = pos3giocatore;
                PlayerPrefs.SetString("pos4name", pos4giocatore);
                pos3giocatore = pos2giocatore;
                PlayerPrefs.SetString("pos3name", pos3giocatore);
                pos2giocatore = ultimogiocatore;
                PlayerPrefs.SetString("pos2name", pos2giocatore);

                //Scalo di una posizione i punteggi 
                pos5punti = pos4punti;
                PlayerPrefs.SetInt("pos5punti", pos5punti);
                pos4punti = pos3punti;
                PlayerPrefs.SetInt("pos4punti", pos4punti);
                pos3punti = pos2punti;
                PlayerPrefs.SetInt("pos3punti", pos3punti);
                pos2punti = ultimopunteggio;
                PlayerPrefs.SetInt("pos2punti", pos2punti);

                //Scalo di una posizione la data
                pos5data = pos4data;
                PlayerPrefs.SetString("pos5data", pos5data);
                pos4data = pos3data;
                PlayerPrefs.SetString("pos4data", pos4data);
                pos3data = pos2data;
                PlayerPrefs.SetString("pos3data", pos3data);
                pos2data = data;
                PlayerPrefs.SetString("pos2data", pos2data);
            }
            else if (ultimopunteggio > pos3punti)
            {
                //Scalo di una posizione i nome giocatore
                pos5giocatore = pos4giocatore;
                PlayerPrefs.SetString("pos5name", pos5giocatore);
                pos4giocatore = pos3giocatore;
                PlayerPrefs.SetString("pos4name", pos4giocatore);
                pos3giocatore = ultimogiocatore;
                PlayerPrefs.SetString("pos3name", pos3giocatore);

                //Scalo di una posizione i punteggi 
                pos5punti = pos4punti;
                PlayerPrefs.SetInt("pos5punti", pos5punti);
                pos4punti = pos3punti;
                PlayerPrefs.SetInt("pos4punti", pos4punti);
                pos3punti = ultimopunteggio;
                PlayerPrefs.SetInt("pos3punti", pos3punti);

                //Scalo di una posizione la data
                pos5data = pos4data;
                PlayerPrefs.SetString("pos5data", pos5data);
                pos4data = pos3data;
                PlayerPrefs.SetString("pos4data", pos4data);
                pos3data = data;
                PlayerPrefs.SetString("pos3data", pos3data);

            }
            else if (ultimopunteggio > pos4punti)
            {
                //Scalo di una posizione i nome giocatore
                pos5giocatore = pos4giocatore;
                PlayerPrefs.SetString("pos5name", pos5giocatore);
                pos4giocatore = ultimogiocatore;
                PlayerPrefs.SetString("pos4name", pos4giocatore);

                //Scalo di una posizione i punteggi 
                pos5punti = pos4punti;
                PlayerPrefs.SetInt("pos5punti", pos5punti);
                pos4punti = ultimopunteggio;
                PlayerPrefs.SetInt("pos4punti", pos4punti);

                //Scalo di una posizione la data
                pos5data = pos4data;
                PlayerPrefs.SetString("pos5data", pos5data);
                pos4data = data;
                PlayerPrefs.SetString("pos4data", pos4data);

            }
            else if (ultimopunteggio > pos5punti)
            {
                // Scalo di una posizione i nome giocatore
                pos5giocatore = ultimogiocatore;
                PlayerPrefs.SetString("pos5name", pos5giocatore);

                //Scalo di una posizione i punteggi 
                pos5punti = ultimopunteggio;
                PlayerPrefs.SetInt("pos5punti", pos5punti);

                //Scalo di una posizione la data
                pos5data = data;
                PlayerPrefs.SetString("pos5data", pos5data);

            }
            //Resetto i dati dell'ultimo giocatore
            PlayerPrefs.DeleteKey("lastname");
            PlayerPrefs.DeleteKey("lastpoint");
            PlayerPrefs.DeleteKey("lastdate");

            //Scrivo fisicamente le posizioni nella classifica 
            gameObject.transform.GetChild(1).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = pos1giocatore;
            gameObject.transform.GetChild(1).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "" + pos1punti;
            gameObject.transform.GetChild(1).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = pos1data;
            gameObject.transform.GetChild(2).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = pos2giocatore;
            gameObject.transform.GetChild(2).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "" + pos2punti;
            gameObject.transform.GetChild(2).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = pos2data;
            gameObject.transform.GetChild(3).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = pos3giocatore;
            gameObject.transform.GetChild(3).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "" + pos3punti;
            gameObject.transform.GetChild(3).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = pos3data;
            gameObject.transform.GetChild(4).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = pos4giocatore;
            gameObject.transform.GetChild(4).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "" + pos4punti;
            gameObject.transform.GetChild(4).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = pos4data;
            gameObject.transform.GetChild(5).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = pos5giocatore;
            gameObject.transform.GetChild(5).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "" + pos5punti;
            gameObject.transform.GetChild(5).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = pos5data;


            //metto una riga vuota anzicchè 0 se non c' è un punteggio
            if (pos1punti == 0)
                gameObject.transform.GetChild(1).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "";
            if (pos2punti == 0)
                gameObject.transform.GetChild(2).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "";
            if (pos3punti == 0)
                gameObject.transform.GetChild(3).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "";
            if (pos4punti == 0)
                gameObject.transform.GetChild(4).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "";
            if (pos5punti == 0)
                gameObject.transform.GetChild(5).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = "";
    }
    public void Reset()
    {
        PlayerPrefs.DeleteAll();
        gameObject.transform.GetChild(1).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(1).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(1).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(2).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(2).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(2).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(3).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(3).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(3).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(4).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(4).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(4).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(5).gameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(5).gameObject.transform.GetChild(2).gameObject.GetComponent<Text>().text = " ";
        gameObject.transform.GetChild(5).gameObject.transform.GetChild(3).gameObject.GetComponent<Text>().text = " ";
    }
}
