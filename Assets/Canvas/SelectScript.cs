﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectScript : MonoBehaviour {
    public GameObject MainMenu;
    public GameObject classifica;
    public GameObject tutorial;
    public GameObject opzioni;
    public AudioSource audioSource;
    public AudioClip SoundHightlighted;
    public AudioClip SoundPlay;
    public AudioClip SoundSelected;
    public AudioClip SoundExit;

    public void Start()
    {
        MainMenu.SetActive(true);
        classifica.SetActive(false);
        tutorial.SetActive(false);
        opzioni.SetActive(false);
    }

    public void Play()
    {
        SceneManager.LoadSceneAsync("Game");
    }

    public void Classifica()
    {
        MainMenu.SetActive(false);
        classifica.SetActive(true);
    }

    public void Tutorial()
    {
        MainMenu.SetActive(false);
        tutorial.SetActive(true);
    }

    public void Opzioni()
    {
        MainMenu.SetActive(false);
        opzioni.SetActive(true);
    }

    public void Indietro()
    {
        MainMenu.SetActive(true);
        opzioni.SetActive(false);
        classifica.SetActive(false);
        tutorial.SetActive(false);
    }

    public void HightlightUI()
    {
        audioSource.PlayOneShot(SoundHightlighted);
    }
    public void SelectUI()
    {
        audioSource.PlayOneShot(SoundSelected);
    }

    public void PllayUI()
    {
        audioSource.PlayOneShot(SoundPlay);
    }

    public void ExitUI()
    {
        audioSource.PlayOneShot(SoundExit);
    }
    
    public void Esci()
    {
        Application.Quit();
    }
}
