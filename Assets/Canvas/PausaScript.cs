﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;


public class PausaScript : MonoBehaviour {

    GameObject player;
    public GameObject opzioni;
    public static bool GameIsPaused = false;
    public GameObject PanelPausa;
    public GameObject PanelPreDeath;
    public GameObject PanelDeath;
    public GameObject PanelGame;
    public GameObject PnlNomeGiocatore;
    public AudioSource audioSource;
    public AudioClip SoundHightlighted;
    public AudioClip SoundPlay;
    public AudioClip SoundSelected;
    public AudioClip SoundExit;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!PanelDeath.activeInHierarchy &&  !opzioni.activeInHierarchy && !PnlNomeGiocatore.activeInHierarchy)
            {
                if (GameIsPaused)
                {
                    Riprendi();
                }
                else
                {
                    Pausa();
                }
            }
        }
    }

    public void Pausa()
    {
        PanelPausa.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        PanelGame.SetActive(false);
        PanelPreDeath.SetActive(false);
        //disabilito i vari componeneti
        GameObject.FindWithTag("arma").GetComponent<AudioSource>().enabled = false;
        GameObject.FindWithTag("arma").GetComponent<Sparare>().enabled = false;
        
        Cursor.visible = true;
        player.GetComponent<Visuale>().enabled = false;
        if (AudioManager.singleton)
        {
            AudioManager.singleton.PauseMusic();
        }

        //sblocco il mouse
        RigidbodyFirstPersonController movementcontroller = player.GetComponent<RigidbodyFirstPersonController>();
        movementcontroller.enabled = false;
        movementcontroller.mouseLook.SetCursorLock(false);
    }

    public void Riprendi()
    {
        PanelPausa.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        PanelGame.SetActive(true);
        //abilito i vari componenti
        GameObject.FindWithTag("arma").GetComponent<AudioSource>().enabled = true;
        GameObject.FindWithTag("arma").GetComponent<Sparare>().enabled = true;
        player.GetComponent<Visuale>().enabled = true;
        if (AudioManager.singleton)
        {
            AudioManager.singleton.UnpauseMusic();
        }

        //blocco il mouse
        RigidbodyFirstPersonController movementcontroller = player.GetComponent<RigidbodyFirstPersonController>();
        movementcontroller.mouseLook.SetCursorLock(true);
        movementcontroller.enabled = true;
    }
    
    public void Opzioni()
    {
        PanelPausa.SetActive(false);
        opzioni.SetActive(true);
    }
    
    public void Ricomincia()
    {
        Destroy(ManagerPlayer.singleton.gameObject);
        Destroy(ManagerMap.singleton.gameObject);
        Destroy(GestioneScene.singleton.gameObject);
        PanelPausa.SetActive(false);
        PanelGame.SetActive(true);
        Time.timeScale = 1f;
        GameIsPaused = false;
        SceneManager.LoadScene("Game");
        if (AudioManager.singleton)
        {
            AudioManager.singleton.UnpauseMusic();
        }

        //abilito i vari componenti
        GameObject.FindWithTag("arma").GetComponent<AudioSource>().enabled = true;
        GameObject.FindWithTag("arma").GetComponent<Sparare>().enabled = true;
        player.GetComponent<Visuale>().enabled = true;

        //blocco il mouse
        RigidbodyFirstPersonController movementcontroller = player.GetComponent<RigidbodyFirstPersonController>();
        movementcontroller.mouseLook.SetCursorLock(true);
        movementcontroller.enabled = true;
    }

    public void TornaAlMenu()
    {
        Destroy(ManagerPlayer.singleton.gameObject);
        Destroy(ManagerMap.singleton.gameObject);
        Destroy(GestioneScene.singleton.gameObject);
        Time.timeScale = 1f;
        GameIsPaused = false;
        SceneManager.LoadScene("MainMenu");
        if (AudioManager.singleton)
        {
            AudioManager.singleton.UnpauseMusic();
        }
    }
    
    public void Indietro()
    {
        PanelPausa.SetActive(true);
        opzioni.SetActive(false);
    }

    public void HightlightUI()
    {
        audioSource.PlayOneShot(SoundHightlighted);
    }
    public void SelectUI()
    {
        audioSource.PlayOneShot(SoundSelected);
    }

    public void PllayUI()
    {
        audioSource.PlayOneShot(SoundPlay);
    }

    public void ExitUI()
    {
        audioSource.PlayOneShot(SoundExit);
    }
}
