﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class TutorialScript : MonoBehaviour
{
    public Vector3 posIniziale;
    public Vector2 WidthIniziale;
    public GameObject img;
    public VideoPlayer video;
    // Start is called before the first frame update
    void Start()
    {
        posIniziale = GetComponent<RectTransform>().localPosition;
        WidthIniziale = GetComponent<RectTransform>().sizeDelta;
        img.SetActive(false);
        video = GetComponentInChildren<VideoPlayer>();
    }

    public void HighlitedEnter()
    {
        transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
    }

    public void HighlitedEXit()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
    }

    public void Seleziona()
    {
        img.SetActive(true);
        video.Play();
    }

    public void ChiudiImmagine()
    {
        img.SetActive(false);
        video.Stop();
    }
}