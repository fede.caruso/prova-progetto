﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestioneScene4 : GestioneScene
{
    // Start is called before the first frame update
    void Start()
    {
        level = 4;
        Nuccisioni = 0;
        NextLevel = GameObject.FindWithTag("PnlNxtLvl").GetComponent<Text>();
        pnlCurrentLevel = GameObject.FindWithTag("PnlCurrentLevel").GetComponent<Text>();
        timer = 60;
        uccisioninecessarie += 4;
        puntoSpawn = puntiSpawn[0].localPosition;
        puntoSpawn2 = GetRandomSpawn().localPosition;
        StartCoroutine(SpawnZombie2());
        NextLevel.enabled = false;
        maxUccisioni = 78;
    }

    void Update()
    {
        pnlCurrentLevel.text = "Level " + level;
        if (StartCountDown)
        {
            NextLevel.enabled = true;
            timer -= Time.deltaTime;
            Debug.Log(timer.ToString("F"));
            NextLevel.text = "PROSSIMO LIVELLO TRA " + timer.ToString("F");
            if (timer < 0.00f)
            {
                timer = 0.00f;
                NextLevel.text = "PROSSIMO LIVELLO TRA " + timer.ToString("F");
            }
        }

        Debug.Log("Nuccisioni " + Nuccisioni);
        if (Nuccisioni == uccisioninecessarie)
        {
            if (Nuccisioni == maxUccisioni)
            {
                StartCountDown = true;
                StopSpawnZombie = true;
                StopCoroutine(SpawnZombie2());
                StartCoroutine(LivelloSuccessivo());
            }

            //faccio passare qualche secondo prima di spawnare altri nemici
            if (StopSpawnZombie == false)
            {
                Nuccisioni = 0;
                uccisioninecessarie += 2;

                puntoSpawn = GetRandomSpawn().localPosition;
                puntoSpawn2 = GetRandomSpawn().localPosition;
                //spawno i nemici in un punto random
                StartCoroutine(SpawnZombie2());
            }
        }
    }


    public IEnumerator SpawnZombie2()
    {
        for (enemyCount = 0; enemyCount < uccisioninecessarie/2; enemyCount++)
        {
            Debug.Log("Sto Spawnando uno Zombie in" + puntoSpawn);
            xPos = Random.Range(-10, 10);
            zPos = Random.Range(-7, 7);

            GameObject zombie = GetRandomZombie();
            Vector3 positionAroundPoint = new Vector3(xPos, 0.1f, zPos) + puntoSpawn;
            Instantiate(zombie, positionAroundPoint, Quaternion.identity);

            GameObject zombie2 = GetRandomZombie();
            Vector3 positionAroundPoint2 = new Vector3(xPos, 0.1f, zPos) + puntoSpawn2;
            Instantiate(zombie2, positionAroundPoint2, Quaternion.LookRotation(ManagerPlayer.singleton.gameObject.transform.forward * -1));
            yield return new WaitForSeconds(2f);
        }
        Debug.Log("Ho finito di spawnare");
        Debug.Log(uccisioninecessarie);
    }

    public IEnumerator LivelloSuccessivo()
    {
        level += 1;
        maxUccisioni += 40;
        Nuccisioni = 0;
        uccisioninecessarie += 2;
        NextLevel.enabled = false;
        StartCountDown = false;
        StopSpawnZombie = false;
        yield return new WaitForSeconds(60f);
    }
}
