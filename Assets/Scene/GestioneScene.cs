﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GestioneScene : MonoBehaviour {

    public static GestioneScene singleton;
    public static int Nuccisioni;
    public static int uccisioninecessarie=1;
    public static int maxUccisioni;
    public float timer;
    public Text NextLevel;
    public Text pnlCurrentLevel;
    public bool StartCountDown = false;
    public bool StopSpawnZombie = false;
    public int level;
    public Transform[] puntiSpawn;
    public GameObject[] Zombies;
    public float xPos;
    public float zPos;
    public static int enemyCount;
    public Vector3 puntoSpawn;
    public Vector3 puntoSpawn2;

    public GameObject GetRandomZombie()
    {
        return Zombies[UnityEngine.Random.Range(0, Zombies.Length)];
    }

    public Transform GetRandomSpawn()
    {
        return puntiSpawn[UnityEngine.Random.Range(0, puntiSpawn.Length)];
    }

    void Awake()
    {
        if ((singleton != null) && (singleton != this))
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
    }

    void Start()
    {
        level = 1;
        Nuccisioni = 0;
        maxUccisioni = 6;
        timer = 20;
        uccisioninecessarie = 1;
        puntoSpawn = puntiSpawn[0].localPosition;
        StartCoroutine(SpawnZombie1());
        NextLevel.enabled = false;
    }

    void Update()
    {
        pnlCurrentLevel.text = "Level " + level;
        if (StartCountDown)
        {
            NextLevel.enabled = true;
            timer -= Time.deltaTime;
            Debug.Log(timer.ToString("F"));
            NextLevel.text = "PROSSIMO LIVELLO TRA " + timer.ToString("F");
            if (timer < 0.00f)
            {
                timer = 0.00f;
                NextLevel.text = "PROSSIMO LIVELLO TRA " + timer.ToString("F");
            }
        }

        Debug.Log("Nuccisioni " + Nuccisioni);
        if (Nuccisioni == uccisioninecessarie)
        {
            if (Nuccisioni == maxUccisioni)
            {
                StartCountDown = true;
                StopSpawnZombie = true;
                StopCoroutine(SpawnZombie1());
                StartCoroutine(Cambiascena());
            }

            //faccio passare qualche secondo prima di spawnare altri nemici
            if (StopSpawnZombie == false)
            {
                Nuccisioni = 0;
                uccisioninecessarie += 1;

                puntoSpawn = GetRandomSpawn().localPosition;
                //spawno i nemici in un punto random
                StartCoroutine(SpawnZombie1());
            }
        }
    }

    public IEnumerator SpawnZombie1()
    {

        for (enemyCount=0; enemyCount<uccisioninecessarie; enemyCount++)
        {
            Debug.Log("Sto Spawnando uno Zombie in"+ puntoSpawn);
            xPos = Random.Range(-7, 7);
            zPos = Random.Range(-7, 7);

            GameObject zombie = GetRandomZombie();
            Vector3 positionAroundPoint = new Vector3(xPos, 0.1f, zPos) + puntoSpawn;
            Instantiate(zombie, positionAroundPoint, Quaternion.LookRotation(ManagerPlayer.singleton.gameObject.transform.forward*-1));
            
            yield return new WaitForSeconds(3f);
        }
        Debug.Log("Ho finito di spawnare");
        Debug.Log(uccisioninecessarie);
    }

    public IEnumerator Cambiascena()
    {
        yield return new WaitForSeconds(20f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
