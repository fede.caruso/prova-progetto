﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GestioneScene2 : GestioneScene
{
    // Start is called before the first frame update
    void Start()
    {
        level = 2;
        Nuccisioni = 0;
        NextLevel = GameObject.FindWithTag("PnlNxtLvl").GetComponent<Text>();
        pnlCurrentLevel = GameObject.FindWithTag("PnlCurrentLevel").GetComponent<Text>();
        timer = 30;
        uccisioninecessarie += 2;
        puntoSpawn = puntiSpawn[0].localPosition;
        StartCoroutine(SpawnZombie2());
        NextLevel.enabled = false;
        maxUccisioni = 14;
    }

    void Update()
    {
        pnlCurrentLevel.text = "Level " + level;
        if (StartCountDown)
        {
            NextLevel.enabled = true;
            timer -= Time.deltaTime;
            Debug.Log(timer.ToString("F"));
            NextLevel.text = "PROSSIMO LIVELLO TRA " + timer.ToString("F");
            if (timer < 0.00f)
            {
                timer = 0.00f;
                NextLevel.text = "PROSSIMO LIVELLO TRA " + timer.ToString("F");
            }
        }

        Debug.Log("Nuccisioni " + Nuccisioni);
        if (Nuccisioni == uccisioninecessarie)
        {
            if (Nuccisioni == maxUccisioni)
            {
                StartCountDown = true;
                StopSpawnZombie = true;
                StopCoroutine(SpawnZombie2());
                StartCoroutine(Cambiascena());
            }

            //faccio passare qualche secondo prima di spawnare altri nemici
            if (StopSpawnZombie == false)
            {
                Nuccisioni = 0;
                uccisioninecessarie += 2;

                puntoSpawn = GetRandomSpawn().localPosition;  //spawno i nemici in un punto random
                StartCoroutine(SpawnZombie2());
            }
        }
    }

    public IEnumerator SpawnZombie2()
    {

        for (enemyCount = 0; enemyCount < uccisioninecessarie; enemyCount++)
        {
            Debug.Log("Sto Spawnando uno Zombie in" + puntoSpawn);
            xPos = Random.Range(-10, 10);
            zPos = Random.Range(-7, 7);
            GameObject zombie = GetRandomZombie();
            Vector3 positionAroundPoint = new Vector3(xPos, 0.1f, zPos) + puntoSpawn;
            Instantiate(zombie, positionAroundPoint, Quaternion.LookRotation(ManagerPlayer.singleton.gameObject.transform.forward * -1));
            yield return new WaitForSeconds(2f);
        }
        Debug.Log("Ho finito di spawnare");
        Debug.Log(uccisioninecessarie);
    }

    new public IEnumerator Cambiascena()
    {
        yield return new WaitForSeconds(30f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
