﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GestioneScene3 : GestioneScene
{
    // Start is called before the first frame update
    void Start()
    {
        level = 3;
        Nuccisioni = 0;
        NextLevel = GameObject.FindWithTag("PnlNxtLvl").GetComponent<Text>();
        pnlCurrentLevel = GameObject.FindWithTag("PnlCurrentLevel").GetComponent<Text>();
        timer = 50;
        uccisioninecessarie += 3;
        puntoSpawn = puntiSpawn[0].localPosition;
        StartCoroutine(SpawnZombie3());
        NextLevel.enabled = false;
        maxUccisioni = 38;
    }
    void Update()
    {
        pnlCurrentLevel.text = "Level " + level;
        if (StartCountDown)
        {
            NextLevel.enabled = true;
            timer -= Time.deltaTime;
            Debug.Log(timer.ToString("F"));
            NextLevel.text = "PROSSIMO LIVELLO TRA " + timer.ToString("F");
            if (timer < 0.00f)
            {
                timer = 0.00f;
                NextLevel.text = "PROSSIMO LIVELLO TRA " + timer.ToString("F");
            }
        }

        Debug.Log("Nuccisioni " + Nuccisioni);
        if (Nuccisioni == uccisioninecessarie)
        {
            if (Nuccisioni == maxUccisioni)
            {
                StartCountDown = true;
                StopSpawnZombie = true;
                StopCoroutine(SpawnZombie3());
                StartCoroutine(Cambiascena());
            }

            //faccio passare qualche secondo prima di spawnare altri nemici
            if (StopSpawnZombie == false)
            {
                Nuccisioni = 0;
                uccisioninecessarie += 3;

                puntoSpawn = GetRandomSpawn().localPosition;  //spawno i nemici in un punto random
                StartCoroutine(SpawnZombie3());
            }
        }
    }

    public IEnumerator SpawnZombie3()
    {

        for (enemyCount = 0; enemyCount < uccisioninecessarie; enemyCount++)
        {
            Debug.Log("Sto Spawnando uno Zombie in" + puntoSpawn);
            xPos = Random.Range(-10, 10);
            zPos = Random.Range(-7, 7);
            GameObject zombie = GetRandomZombie();
            Vector3 positionAroundPoint = new Vector3(xPos, 0.1f, zPos) + puntoSpawn;
            Instantiate(zombie, positionAroundPoint, Quaternion.LookRotation(ManagerPlayer.singleton.gameObject.transform.forward * -1));
            yield return new WaitForSeconds(1f);
        }
        Debug.Log("Ho finito di spawnare");
        Debug.Log(uccisioninecessarie);
    }

    new public IEnumerator Cambiascena()
    {
        yield return new WaitForSeconds(50f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
