﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LampeggiaScript : MonoBehaviour {
    Text _lbl;
    public float BlinkFadeInTime = 0.5f;
    public float BlinkStayTime = 0.5f;
    public float BlinkFadeOutTime = 0.7f;
    public float _timeCheker = 0;
    private Color _color;
    // Use this for initialization
    void Start () {
        _lbl = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        _timeCheker += Time.deltaTime;
        if (_timeCheker < BlinkFadeInTime)
        {
            _lbl.color = new Color(130, 6, 6, _timeCheker / BlinkFadeInTime);
        }
        else if (_timeCheker < BlinkFadeInTime + BlinkStayTime)
        {
            _lbl.color = new Color(130, 6, 6, 1);
        }
        else if (_timeCheker < BlinkFadeInTime + BlinkStayTime + BlinkFadeOutTime)
        {
            _lbl.color = new Color(130, 6, 6, 1 - (_timeCheker - (BlinkFadeInTime + BlinkStayTime)) / BlinkFadeOutTime);
        }
        else
        {
            _timeCheker = 0;
        }
    }
}
