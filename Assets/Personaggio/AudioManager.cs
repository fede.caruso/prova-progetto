﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager singleton;
    
    void Awake()
    {
        
        //autodistruggo la Mappa nel caso in cui se ne dovese spawnare un altra
        if ((singleton != null) && (singleton != this))
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
        DontDestroyOnLoad(gameObject);
    }

    public void PlayMusic()
    {
        GetComponent<AudioSource>().Play();
    }

    public void PauseMusic()
    {
        GetComponent<AudioSource>().Pause();
    }

    public void UnpauseMusic()
    {
        GetComponent<AudioSource>().UnPause();
    }
}
