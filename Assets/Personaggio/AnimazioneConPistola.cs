﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimazioneConPistola : MonoBehaviour
{
    Animator anim;
    GameObject spalla;
    GameObject pistola;
    ManagerPlayer managerplayer;
    public Vector3 Offset;
    public AudioClip[] clips;
    public AudioSource audioSource;
    public Transform Target;
    public Transform pistol;
    public Transform spine;
    public Quaternion rotazione;

    // Use this for initialization
    void Start()
    {
        managerplayer = GetComponentInParent<ManagerPlayer>();
        anim = GetComponent<Animator>();
        pistola = GameObject.FindWithTag("arma");
        audioSource = GetComponent<AudioSource>();
        rotazione = pistol.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        //camminata
        if (Input.GetAxis("Vertical") > 0)
           anim.SetBool("cammina", true);
        else if (Input.GetAxis("Vertical") == 0)
             anim.SetBool("cammina", false);

        //corsa
        if (Input.GetKey(KeyCode.LeftShift)&& Input.GetAxis("Vertical") > 0)
           anim.SetBool("corre", true);
        else if (Input.GetKeyUp(KeyCode.LeftShift))
            anim.SetBool("corre", false);
        else if (Input.GetAxis("Vertical") == 0)
          anim.SetBool("corre", false);

        //indietro
        if (Input.GetAxis("Vertical") < 0)
            anim.SetBool("indietro", true);
        else if (Input.GetAxis("Vertical") == 0)
            anim.SetBool("indietro", false);

        //cammina destra
        if (Input.GetAxis("Horizontal") > 0)
            anim.SetBool("cammina DX", true);
        else if (Input.GetAxis("Horizontal") == 0)
            anim.SetBool("cammina DX", false);

        //cammina sinistra
        if (Input.GetAxis("Horizontal") < 0)
            anim.SetBool("cammina SX", true);
        else if (Input.GetAxis("Horizontal") == 0)
            anim.SetBool("cammina SX", false);

        //salta
        if (Input.GetKeyDown(KeyCode.Space))
            anim.SetBool("salta", true);
        else 
            anim.SetBool("salta", false);

        //sparo
        if (ManagerPlayer.isLoad)
        {
            if (Input.GetMouseButtonDown(0))
                pistol.Rotate(-Vector3.forward, 2f);
            else
                pistol.localRotation = rotazione;
        }

        //morte
        if (managerplayer.health <= 0)
            anim.SetBool("morte", true); 

    }

    void LateUpdate()
    {
        if (managerplayer.health > 0)
        {
            spine.LookAt(Target.position);
            spine.rotation = spine.rotation * Quaternion.Euler(Offset);
        }
    }

    public void Step()
    {
        AudioClip clip = GetRandomClip();
        audioSource.PlayOneShot(clip);
    }

    public void Salto()
    {
        AudioClip clip = clips[2];
        audioSource.PlayOneShot(clip);
    }

    public AudioClip GetRandomClip()
    {
        return clips[UnityEngine.Random.Range(0, 1)];
    }
}