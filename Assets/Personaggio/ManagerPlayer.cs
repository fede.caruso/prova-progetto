﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class ManagerPlayer : MonoBehaviour {

    Animator anim;
    GameObject arma;
    public static ManagerPlayer singleton;  //imposto il Player come singleton
    float maxhealth;
    public float health;
    public int point;
    public static int money;
    public static int ammo;
    public static bool isLoad;
    public Text NomeGiocatore;
    public Slider healthslider;
    public GameObject PanelGame;
    public GameObject PanelpreDeath;
    public GameObject PanelDeath;
    public GameObject PanelPause;
    public GameObject PanelNomeGiocatore;
    public GameObject camdeath;
    public Text lblPoint;
    public Text lblMoney;
    public Text lblFinalPoint;
    public Text lblNomeGiocatore;
    public Text lblMunizioni;
    
    public float currenthealthpercentage
    {
        get
        {
            return (health / maxhealth);
        }
    }

    void Awake()
    {
        //autodistruggo il giocatore nel caso in cui se ne dovese spawnare un altro
        if((singleton != null) && (singleton != this))
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
        //non distruggo l' oggetto Player al caricamento di una nuova scena
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        maxhealth = health;
        money = 0;
        ammo = 65;
        healthslider.enabled = true;
        PanelpreDeath.SetActive(false);
        PanelDeath.SetActive(false);
        anim = GetComponentInChildren<Animator>();
        camdeath.SetActive(false);
        arma = GameObject.FindGameObjectWithTag("arma");
        lblPoint.text = "Point: " + point;
        InserisciNome();
    }   
	// Update is called once per frame
	void Update () {
        if (health > maxhealth)
            health = maxhealth;

        HealthhBar();
        MortePersonaggio();
        if (healthslider.value > 0.25f)
        {
            PanelpreDeath.SetActive(false);
        }
        //controllo munizioni
        if (ammo > 0)
        {
            isLoad = true;
        }
        else
        {
            ammo = 0;
            isLoad = false;
        }
            
    }

    public void PlayGame()
    {
        lblNomeGiocatore.text = NomeGiocatore.text;
        PanelNomeGiocatore.SetActive(false);
        Time.timeScale = 1f;
        PausaScript.GameIsPaused = false;
        PanelGame.SetActive(true);
        //abilito i vari componenti
        arma.GetComponent<AudioSource>().enabled = true;
        arma.GetComponent<Sparare>().enabled = true;
        GetComponent<Visuale>().enabled = true;

        //blocco il mouse
        RigidbodyFirstPersonController movementcontroller = GetComponent<RigidbodyFirstPersonController>();
        movementcontroller.mouseLook.SetCursorLock(true);
        movementcontroller.enabled = true;
        
    }

    public void InserisciNome()
    {
            PanelNomeGiocatore.SetActive(true);
            Time.timeScale = 0f;
            PausaScript.GameIsPaused = true;
            PanelGame.SetActive(false);
            //disabilito i vari componeneti
            arma.GetComponent<AudioSource>().enabled = false;
            arma.GetComponent<Sparare>().enabled = false;
            Cursor.visible = true;
            GetComponent<Visuale>().enabled = false;

            //sblocco il mouse
            RigidbodyFirstPersonController movementcontroller = GetComponent<RigidbodyFirstPersonController>();
            movementcontroller.enabled = false;
            movementcontroller.mouseLook.SetCursorLock(false);
    }

    public void savepoint()
    {
        PlayerPrefs.SetString("lastname", NomeGiocatore.text);
        PlayerPrefs.SetInt("lastpoint", point);
        PlayerPrefs.SetString("lastdate", System.DateTime.Now.ToString("dd/MM/yyyy"));
    }

    public void HealthhBar()
    {
        lblPoint.text = "Point: " + point;
        lblMoney.text = "€" + money;
        lblMunizioni.text ="" + ammo;
        healthslider.value = currenthealthpercentage;

        if(healthslider.value > 0.75)
        {
            healthslider.fillRect.GetComponent<Image>().color = new Color(0.11f, 0.93f, 0f);
        }
        else if (healthslider.value <= 0.75 && healthslider.value > 0.50)
        {
            healthslider.fillRect.GetComponent<Image>().color = new Color(0.68f, 1.0f, 0.18f);
        }
        else if (healthslider.value <= 0.50 && healthslider.value > 0.25)
        {
            healthslider.fillRect.GetComponent<Image>().color = new Color(1.0f, 0.4f, 0.0f);
        }
        else if (healthslider.value <= 0.25 && PausaScript.GameIsPaused == false)
        {
            healthslider.fillRect.GetComponent<Image>().color = new Color(1.0f, 0.0f, 0.0f);

            PanelpreDeath.SetActive(true);
        }
    }

    public void MortePersonaggio()
    {
        if (health <= 0)
        {
            camdeath.SetActive(true);
            savepoint();
            if (AggiornaClassifica.singleton)
            {
                AggiornaClassifica.singleton.Aggiorna();
            }
            PanelGame.SetActive(false);
            PanelDeath.SetActive(true);

            //sblocco il mouse
            RigidbodyFirstPersonController movementcontroller = GetComponent<RigidbodyFirstPersonController>();
            movementcontroller.enabled = false;
            movementcontroller.mouseLook.SetCursorLock(false);

            //ottengo il punteggio
            lblFinalPoint.text = lblNomeGiocatore.text + " " + point;

            GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController>().enabled = false;
            PanelpreDeath.GetComponent<AudioSource>().Stop();

            //disabilito camere
            GetComponent<Visuale>().enabled = false;

            //disabilito script arma
            GameObject.FindWithTag("arma").GetComponent<AudioSource>().enabled = false;
            GameObject.FindWithTag("arma").GetComponent<Sparare>().enabled = false;

            //disattivo animator sparo
            anim.SetLayerWeight(1, 0f);

            //allontano la camera alla morte
            if (camdeath.transform.position.y <= 3)
            {
                camdeath.transform.Translate(Vector3.back * 0.8f * Time.deltaTime);
                camdeath.transform.Translate(Vector3.up * 0.2f * Time.deltaTime);
            }
        }
    }
}
