﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Visuale : MonoBehaviour {
    public GameObject MainCamera;
    public GameObject CameraMirino;
    public GameObject mirino;
    RigidbodyFirstPersonController movementcontroller;

    // Use this for initialization
    Vector3 posiniziale;
    Camera cam;

    private void Start()
    {
        movementcontroller = gameObject.GetComponent<RigidbodyFirstPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            MainCamera.transform.position = CameraMirino.transform.position;
            mirino.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            movementcontroller.mouseLook.XSensitivity = 1f;
            movementcontroller.mouseLook.YSensitivity = 1f;
            if (Input.GetMouseButton(0))
            {
                mirino.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            }
        }
        else if (Input.GetMouseButtonUp(1))
        {
            MainCamera.transform.position = MainCamera.transform.position;
            mirino.transform.localScale = new Vector3(2f, 2f, 2f);
            movementcontroller.mouseLook.XSensitivity = 2f;
            movementcontroller.mouseLook.YSensitivity = 2f;
        }
    }
}