﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepClimb : MonoBehaviour
{
    Rigidbody rigidBody;
    [SerializeField] GameObject stepRayUpper;
    [SerializeField] GameObject stepRayLower;
    [SerializeField] GameObject stepRayMedium;
    [SerializeField] GameObject stepRayUpperDx;
    [SerializeField] GameObject stepRayLowerDx;
    [SerializeField] GameObject stepRayMediumDx;
    [SerializeField] GameObject stepRayUpperBk;
    [SerializeField] GameObject stepRayLowerBk;
    [SerializeField] GameObject stepRayMediumBk;
    [SerializeField] GameObject stepRayUpperSx;
    [SerializeField] GameObject stepRayLowerSx;
    [SerializeField] GameObject stepRayMediumSx;
    [SerializeField] float stepSmooth = 2f;
    [SerializeField] float stepHeight = 2f;
    public LayerMask IgnoreMe;
    
    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        IgnoreMe = (1 << LayerMask.NameToLayer("Enemy"));
        stepRayUpper.transform.position = new Vector3(stepRayUpper.transform.position.x, stepHeight, stepRayUpper.transform.position.z);
    }

    private void FixedUpdate()
    {
        stepClimb();
        stepClimbDx();
        stepClimbSx();
        stepClimbBk();
    }

    //Avanti
    void stepClimb()
    {
        RaycastHit hitLower;
        if (Physics.Raycast(stepRayLower.transform.position, transform.TransformDirection(Vector3.forward), out hitLower, 0.1f, ~IgnoreMe))
        {
            RaycastHit hitUpper;
            if (!Physics.Raycast(stepRayUpper.transform.position, transform.TransformDirection(Vector3.forward), out hitUpper, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime * 0.15f, 0f);
            }
        }
        RaycastHit hitMedium;
        if (Physics.Raycast(stepRayMedium.transform.position, transform.TransformDirection(Vector3.forward), out hitMedium, 0.1f, ~IgnoreMe))
        {
            RaycastHit hitUpper;
            if (!Physics.Raycast(stepRayUpper.transform.position, transform.TransformDirection(Vector3.forward), out hitUpper, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime * 0.15f, 0f);
            }
        }

        RaycastHit hitLower45;
        if (Physics.Raycast(stepRayLower.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitLower45, 0.1f, ~IgnoreMe))
        {

            RaycastHit hitUpper45;
            if (!Physics.Raycast(stepRayUpper.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitUpper45, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime * 0.15f, 0f);
            }
        }

        RaycastHit hitLowerMinus45;
        if (Physics.Raycast(stepRayLower.transform.position, transform.TransformDirection(-1.5f, 0, 1), out hitLowerMinus45, 0.1f, ~IgnoreMe))
        {

            RaycastHit hitUpperMinus45;
            if (!Physics.Raycast(stepRayUpper.transform.position, transform.TransformDirection(-1.5f, 0, 1), out hitUpperMinus45, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime * 0.15f, 0f);
            }
        }
    }

    //Destra
    void stepClimbDx()
    {
        RaycastHit hitLower;
        if (Physics.Raycast(stepRayLowerDx.transform.position, transform.TransformDirection(Vector3.right), out hitLower, 0.1f, ~IgnoreMe))
        {
            RaycastHit hitUpper;
            if (!Physics.Raycast(stepRayUpperDx.transform.position, transform.TransformDirection(Vector3.right), out hitUpper, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }
        RaycastHit hitMedium;
        if (Physics.Raycast(stepRayMediumDx.transform.position, transform.TransformDirection(Vector3.right), out hitMedium, 0.1f, ~IgnoreMe))
        {
            RaycastHit hitUpper;
            if (!Physics.Raycast(stepRayUpperDx.transform.position, transform.TransformDirection(Vector3.right), out hitUpper, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }

        RaycastHit hitLower45;
        if (Physics.Raycast(stepRayLowerDx.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitLower45, 0.1f, ~IgnoreMe))
        {

            RaycastHit hitUpper45;
            if (!Physics.Raycast(stepRayUpperDx.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitUpper45, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }

        RaycastHit hitLowerMinus45;
        if (Physics.Raycast(stepRayLowerDx.transform.position, transform.TransformDirection(-1.5f, 0, 1), out hitLowerMinus45, 0.1f, ~IgnoreMe))
        {

            RaycastHit hitUpperMinus45;
            if (!Physics.Raycast(stepRayUpperDx.transform.position, transform.TransformDirection(-1.5f, 0, 1), out hitUpperMinus45, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }
    }
    
    //Sinistra
    void stepClimbSx()
    {
        RaycastHit hitLower;
        if (Physics.Raycast(stepRayLowerSx.transform.position, transform.TransformDirection(Vector3.left), out hitLower, 0.1f, ~IgnoreMe))
        {
            RaycastHit hitUpper;
            if (!Physics.Raycast(stepRayUpperSx.transform.position, transform.TransformDirection(Vector3.left), out hitUpper, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }
        RaycastHit hitMedium;
        if (Physics.Raycast(stepRayMediumSx.transform.position, transform.TransformDirection(Vector3.left), out hitMedium, 0.1f, ~IgnoreMe))
        {
            RaycastHit hitUpper;
            if (!Physics.Raycast(stepRayUpperSx.transform.position, transform.TransformDirection(Vector3.left), out hitUpper, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }

        RaycastHit hitLower45;
        if (Physics.Raycast(stepRayLowerSx.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitLower45, 0.1f, ~IgnoreMe))
        {

            RaycastHit hitUpper45;
            if (!Physics.Raycast(stepRayUpperSx.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitUpper45, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }

        RaycastHit hitLowerMinus45;
        if (Physics.Raycast(stepRayLowerSx.transform.position, transform.TransformDirection(-1.5f, 0, 1), out hitLowerMinus45, 0.1f, ~IgnoreMe))
        {

            RaycastHit hitUpperMinus45;
            if (!Physics.Raycast(stepRayUpperSx.transform.position, transform.TransformDirection(-1.5f, 0, 1), out hitUpperMinus45, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }
    }

    //Dietro
    void stepClimbBk()
    {
        RaycastHit hitLower;
        if (Physics.Raycast(stepRayLowerBk.transform.position, transform.TransformDirection(Vector3.back), out hitLower, 0.1f, ~IgnoreMe))
        {
            RaycastHit hitUpper;
            if (!Physics.Raycast(stepRayUpperBk.transform.position, transform.TransformDirection(Vector3.back), out hitUpper, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }
        RaycastHit hitMedium;
        if (Physics.Raycast(stepRayMediumBk.transform.position, transform.TransformDirection(Vector3.back), out hitMedium, 0.1f, ~IgnoreMe))
        {
            RaycastHit hitUpper;
            if (!Physics.Raycast(stepRayUpperBk.transform.position, transform.TransformDirection(Vector3.back), out hitUpper, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }

        RaycastHit hitLower45;
        if (Physics.Raycast(stepRayLowerBk.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitLower45, 0.1f, ~IgnoreMe))
        {

            RaycastHit hitUpper45;
            if (!Physics.Raycast(stepRayUpperBk.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitUpper45, 0.2f, ~IgnoreMe))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }
    }
}