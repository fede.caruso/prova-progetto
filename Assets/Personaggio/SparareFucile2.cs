﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparareFucile2 : SparareFucile {

    new public void OnEnable()
    {
        StartCoroutine(SparoContinuo());
    }
    void Start()
    {
        hittableMask = ~(1 << 10);
        maskNoenemy = ~((1 << 8) | (1 << 10));
        arma = gameObject;
        particellearma = arma.GetComponentInChildren<ParticleSystem>();
        managerplayer = GetComponentInParent<ManagerPlayer>();
        fucile = GameObject.FindWithTag("arma");
        audiofucile = arma.GetComponent<AudioSource>();
    }

    IEnumerator SparoContinuo()
    {
        while (true)
        {
            premuto = false;
            RaycastHit hit;

            if (Input.GetMouseButton(0))
            {
                premuto = true;
                if (ManagerPlayer.isLoad)
                {
                    audiofucile.Play();
                    Debug.Log("Sto sparando");
                    particellearma.Play();
                    ManagerPlayer.ammo -= 1;

                    if (Physics.Raycast(camTransform.position, camTransform.forward, out hit, distance, hittableMask, QueryTriggerInteraction.Ignore))
                    {
                        Debug.Log("Hai colpito: " + hit.collider.gameObject);
                        Rigidbody colRigidbody = hit.collider.gameObject.GetComponent<Rigidbody>();

                        Enemy hitEnemy = hit.collider.gameObject.GetComponent<Enemy>();

                        if (hitEnemy)
                        {
                            hitEnemy.health = hitEnemy.health - damage;

                            if (hitEnemy.health >= 0)
                            {
                                if (managerplayer)
                                {
                                    //schizzo di sangue
                                    hitEnemy.sangue.transform.position = hit.point;
                                    hitEnemy.sangue.Play();

                                    managerplayer.point = hitEnemy.point + managerplayer.point;
                                }
                            }
                        }
                    }
                    if (Physics.Raycast(camTransform.position, camTransform.forward, out hit, distance, maskNoenemy, QueryTriggerInteraction.Ignore))
                    {
                        Instantiate(BucoProiettile, hit.point, Quaternion.LookRotation(hit.normal));
                    }
                }
            }
            yield return new WaitForSeconds(0.12f);
        }
    }
}