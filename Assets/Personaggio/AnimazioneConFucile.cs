﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimazioneConFucile : MonoBehaviour
{
    Animator anim;
    GameObject spalla;
    GameObject fucile;
    ManagerPlayer managerplayer;
    public Vector3 Offset;
    public Transform Target;
    public Transform spine;
    public AudioClip[] clips;
    public AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        managerplayer = GetComponentInParent<ManagerPlayer>();
        anim = GetComponent<Animator>();
        fucile = GameObject.FindWithTag("arma");
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //camminata
        if (Input.GetAxis("Vertical") > 0)
        {
            anim.SetBool("cammina", true);
            Offset.z = 10f;
        }
        else if (Input.GetAxis("Vertical") == 0)
        {
            anim.SetBool("cammina", false);
            Offset.z = 20f;
        }

        //corsa
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetAxis("Vertical") > 0)
            anim.SetBool("corre", true);
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            anim.SetBool("corre", false);
            Offset.z = 20f;
        } 
        else if (Input.GetAxis("Vertical") == 0)
          anim.SetBool("corre", false);
          
        //indietro
        if (Input.GetAxis("Vertical") < 0)
            anim.SetBool("indietro", true);
        else if (Input.GetAxis("Vertical") == 0)
            anim.SetBool("indietro", false);

        //cammina destra
        if (Input.GetAxis("Horizontal") > 0)
            anim.SetBool("cammina DX", true);

        else if (Input.GetAxis("Horizontal") == 0)
            anim.SetBool("cammina DX", false);

        //cammina sinistra
        if (Input.GetAxis("Horizontal") < 0)
            anim.SetBool("cammina SX", true);
            
        else if (Input.GetAxis("Horizontal") == 0)
            anim.SetBool("cammina SX", false);

        //salta
        if (Input.GetKeyDown(KeyCode.Space))
            anim.SetTrigger("salta");
        else 
            anim.SetBool("salta", false);

        //sparo
        if (Input.GetMouseButton(0))
        {
            if (ManagerPlayer.isLoad)
                anim.SetBool("shoot", true);
        }
        else
            anim.SetBool("shoot", false);

        //morte
        if (managerplayer.health <= 0)
            anim.SetBool("morte", true);
    }

    void LateUpdate()
    {
        if (managerplayer.health > 0)
        {
            spine.LookAt(Target.position);
            spine.rotation = spine.rotation * Quaternion.Euler(Offset);
        }
    }

    public void Step()
    {
        AudioClip clip = GetRandomClip();
        audioSource.PlayOneShot(clip);
    }

    public void Salto()
    {
        AudioClip clip = clips[2];
        audioSource.PlayOneShot(clip);
    }

    public AudioClip GetRandomClip()
    {
        return clips[UnityEngine.Random.Range(0, 1)];
    }
}