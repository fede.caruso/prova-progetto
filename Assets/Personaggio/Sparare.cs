﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sparare : MonoBehaviour
{
    AudioSource audiopistola;
    public GameObject arma;
    public int damage;
    public float distance;
    public float forzaproiettile;
    public Transform camTransform;
    public LineRenderer lineRender;
    public ParticleSystem particellearma;
    public AudioSource NoAmmo;
    public ParticleSystem cartucce;
    public GameObject BucoProiettile;
    public ManagerPlayer managerplayer;
    public LayerMask hittableMask;
    public LayerMask maskNoenemy;
    public GameObject mirino;

    // Use this for initialization
    void Start () {
        hittableMask = ~(1 << 10);
        maskNoenemy = ~((1 << 8) | (1 << 10));
        arma = gameObject;
        particellearma = arma.GetComponentInChildren<ParticleSystem>();
        managerplayer = GetComponentInParent<ManagerPlayer>();
        audiopistola = arma.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (managerplayer.health == 0)
        {
            this.enabled = false;
        }
        SistemaSparo();
    }

    public virtual void SistemaSparo()
    {
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
            if (ManagerPlayer.isLoad)
            {
                particellearma.Play();
                audiopistola.Play();
                cartucce.Play();
                ManagerPlayer.ammo -= 1;
                Debug.Log("Munizioni: " + ManagerPlayer.ammo);

                if (Physics.Raycast(camTransform.position, camTransform.forward, out hit, distance, hittableMask, QueryTriggerInteraction.Ignore))
                {
                    Debug.Log("Hai colpito: " + hit.collider.gameObject.name);
                    Rigidbody colRigidbody = hit.collider.gameObject.GetComponent<Rigidbody>();
                    Enemy hitEnemy = hit.collider.gameObject.GetComponent<Enemy>();

                    if (hitEnemy)
                    {
                        hitEnemy.health = hitEnemy.health - damage;

                        if (hitEnemy.health >= 0)
                        {
                            if (managerplayer)
                            {
                                //schizzo di sangue
                                hitEnemy.sangue.transform.position = hit.point;
                                hitEnemy.sangue.Play();

                                managerplayer.point = hitEnemy.point + managerplayer.point;
                            }
                        }
                    }
                }
                if (Physics.Raycast(camTransform.position, camTransform.forward, out hit, distance, maskNoenemy, QueryTriggerInteraction.Ignore))
                {
                    Instantiate(BucoProiettile, hit.point, Quaternion.LookRotation(hit.normal));
                }
            }
            else
                NoAmmo.Play(); 
        }
    }
}