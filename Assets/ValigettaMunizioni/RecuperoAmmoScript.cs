﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecuperoAmmoScript : MonoBehaviour
{
    bool goUp;
    public int value;
    AudioSource audioRicarica;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SwitchDirection());
        audioRicarica = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (goUp)
        {
            transform.position = transform.position + new Vector3(0, 0.15f * Time.deltaTime, 0);
        }
        else
        {
            transform.position = transform.position - new Vector3(0, 0.15f * Time.deltaTime, 0);
        }  
    }

    IEnumerator SwitchDirection()
    {
        while (gameObject.activeSelf)
        {
            yield return new WaitForSeconds(1f);
            goUp = !goUp;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            OnPicked(other);
        }
    }

    public virtual void OnPicked(Collider other)
    {
        ManagerPlayer.ammo += value;
        audioRicarica.Play();
        //prima li disattivo per poter sentire il suono
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        gameObject.transform.GetChild(1).gameObject.SetActive(false);
        gameObject.transform.GetChild(2).gameObject.SetActive(false);
        gameObject.transform.GetChild(3).gameObject.SetActive(false);
        Invoke("distruggi", 1f);
    }
    void distruggi()
    {
        Destroy(gameObject);
    }
}
