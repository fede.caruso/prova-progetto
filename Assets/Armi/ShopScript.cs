﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopScript : MonoBehaviour
{
    public GameObject PanelShop;
    public GameObject PanelChange;
    public bool Shoped = false;
    public int Price;
    public GameObject PersonaggioPistola;
    public GameObject PersonaggioFucile;
    public GameObject PersonaggioFucile2;
    public GameObject PersonaggioFucile3;
    public GameObject Light;
    public AudioClip suonoCambio;
    public AudioClip suonoCambioNegato;
    public AudioSource audioSource;
    public bool entrato;
    
    // Start is called before the first frame update
    void Start()
    {
        PersonaggioPistola.SetActive(true);
        PersonaggioFucile.SetActive(false);
        PersonaggioFucile2.SetActive(false);
        PersonaggioFucile3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (PausaScript.GameIsPaused)
        {
            PanelShop.SetActive(false);
            PanelChange.SetActive(false);
        }
    }

    public virtual void OnTriggerStay(Collider other)
    { 
        if (other.CompareTag("Player"))
        {
            Light.SetActive(false);
            if (Shoped)
            {
                PanelChange.SetActive(true);
                if (Input.GetKeyUp(KeyCode.C))
                {
                    change();
                }
            }
            else
            {
                PanelShop.SetActive(true);
                if (Input.GetKeyUp(KeyCode.C))
                {
                    shop();
                }
            }
        }
    }

    public virtual void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Light.SetActive(true);
            PanelShop.SetActive(false);
            PanelChange.SetActive(false);
        }
    }

    public virtual void shop()
    {
        
    }

    public virtual void change()
    {
        
    }

    public void SuonoCambia()
    {
        audioSource.PlayOneShot(suonoCambio);
    }

    public void SuonoNega()
    {
        audioSource.PlayOneShot(suonoCambioNegato);
        
    }
}