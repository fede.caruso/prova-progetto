﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop3 : ShopScript
{
    public override void shop()
    {
        if (ManagerPlayer.money >= Price)
        {
            Shoped = true;
            PanelShop.SetActive(false);
            ManagerPlayer.money -= Price;
            PersonaggioPistola.SetActive(false);
            PersonaggioFucile2.SetActive(false);
            PersonaggioFucile.SetActive(false);
            PersonaggioFucile3.SetActive(true);
            SuonoCambia();
        }
        else
            SuonoNega();
    }

    public override void change()
    {
        PersonaggioPistola.SetActive(false);
        PersonaggioFucile2.SetActive(false);
        PersonaggioFucile.SetActive(false);
        PersonaggioFucile3.SetActive(true);
        SuonoCambia();
    }
}
